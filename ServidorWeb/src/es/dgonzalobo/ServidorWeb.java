/**
 * @author dgonzalobo
 * (Desarrollado en UNIX)
 * 
 * PRACTICA 5
 * Se pretende desarrollar un servidor web que atienda peticiones de modo secuencial, a través de un pool de 10 esclavos
 * que esten constantemente comprobadno un buffer donde se almacenan las peticiones que el maestro va esuchando.
 * 
 * Se sugiere utilizar un control de sincronización del buffer donde almacenar las peticiones a través de wait y notifyAll
 * No se utiliza el PUERTO 80, porque en Mac ese puerto esta reservado.
 * 
 * MEJORA 1
 * 	 Lista de forma recursiva, el contenido del servidor, a través de un fichero HTML.
 * 
 * MEJORA 2
 * 	 En el caso de que en la lista que hace de buffer, se acumelen peticiones, se crearán hilos auxiliares para atenderlas, 
 * 	 dichos hilos moriran una vez atendida la petición.
 */
package es.dgonzalobo;

import java.sql.Date;
import java.util.LinkedList;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;


public class ServidorWeb {
	private ServerSocket serverSocket;
	private Socket socket;
	private static int PUERTO;
	private static ServidorWeb server;
	private static String fileIndex;
	private static File dir;
	LinkedList<Socket> pool;

	/**
	 * @funcion si el directorio introducido no es correcto, se informa y se introduce el directorio raiz.
	 * 			se crea la lista LinkedList que hara de buffer del Socket y se arrancará el serverSocket en 
	 * 			el puerto donde se deba escuche.
	 */
	public ServidorWeb(){
		try{
			if (!dir.isDirectory()) {
				System.out.println(dir + " no es un directorio."); 
				dir=new File("./");
			}
			pool = new LinkedList<Socket>();
			serverSocket=new ServerSocket(PUERTO);
		}
		catch(Exception e){
			System.err.println("Fallo en el constructor de la clase ServidorWeb "+e);
		}
	}

	/**
	 * @funcion si se han métido parametros se adoptan, en el caso contrario, se añaden por defecto.
	 * 			Posteriormente se crea el objeto server, compartido con todos los hilos que haran de esclavos.
	 * 			Se invoca el run donde se dará comienzo a la funcionalidad del programa.
	 * @param args[0] directorio sobre el que corre el servidor
	 * @param args[1] puerto al que conectarse
	 * @param args[2] nombre del archivo a 
	 */
	public static void main (String []args){
		if(args.length<0){
			dir=new File(args[0]);
			PUERTO=Integer.parseInt(args[1]);
			if (PUERTO<0||PUERTO>65535) 
				PUERTO=80;
			if (args.length==3) 
				fileIndex=args[2];
		}
		else{
			PUERTO=1080;	
			dir=new File("./");
			fileIndex="index.html";
		}
		server=new ServidorWeb();
		server.run();
	}

	/**
	 * @funcion Arranca los 10 hilos que harán de esclavos durante la vida del servidor, a través de la invocacion
	 * 			de nuevos objetos Thread que apuntan al objeto runnable ThreadServidorWeb
	 * 			Despues sincronziando el acceso a la lista, teniendo como testigo para el acceso el propio objeto pool.
	 * 			En el caso de que se hayan almacenado más de dos peticiones en el buffer, se creará un hilo auxiliar de vida
	 *          igual a la petición, a fin de dar servicio a la demanda. 
	 *          Se añade el socket de la conexion detectada en el buffer, y se notifica la introduccion de un nuevo objeto 
	 *          a la lista a todos los esclavos.
	 */
	public void run(){
		System.out.println("\n  --------------------------------------------------------------------");
		System.out.println("  \t\t\t SERVIDOR WEB. ");
		System.out.println("  \t\t\t(PUERTO: "+PUERTO+")");
		System.out.println("  -------------------------------------------------------------------- \n");
		try{	
			for (int i=0;i<10;i++) { 
				Thread t=new Thread(new ThreadServidorWeb (dir, fileIndex,"ThreadServer "+(i+1),server,true));
				t.start(); 
			}
			while(true){
				socket=serverSocket.accept();
				System.out.println("Detectada Conexion en "+socket.getInetAddress());
				synchronized(pool){
					if(pool.size()>2){
						Thread t=new Thread(new ThreadServidorWeb (dir, fileIndex,"ThreadServer auxiliar",server,false));
						t.start(); 
					}
					pool.add(socket);
					pool.notifyAll();
				}
			}
		}
		catch(NullPointerException e1){
			System.err.println("Otro Servidor está ejecutandose sobre el mismo puerto. Se cierra el server");
			return;
		}
		catch(IOException e2){
			System.err.println("Error durante la escucha. Se cierra el server.");
			return;
		}
		catch(Exception e){
			System.err.println("Error en el Run "+e);
		}
	}

}

class ThreadServidorWeb implements Runnable{
	private Socket socket;
	private ServidorWeb server;
	private String name,get,root,indexFileName= "index.html"; 
	private File sourceFile;
	private PrintWriter out;
	private InputStreamReader in;
	private BufferedOutputStream raw;
	private BufferedReader entrada;
	private boolean flag,keepAlife;

	/**
	 * @funcion Se inicializan las variables abajo indicadas, con los parametros recibidos en la construcción de hilo.
	 * @param bDir directorio sobre le que corre el servidor
	 * @param indexFich nombre del fichero index que en caso de no tener nada en el get del http se ha de mostrar
	 * @param n nombre del hilo
	 * @param serv objeto servidorWeb, a través del cual se compartirán recursos con el resto de esclavos
	 * @param b flag para indicar si es un esclavo fijo o esclavo auxiliar de vida limitada a la petición
	 */
	public ThreadServidorWeb(File bDir, String indexFich, String n, ServidorWeb serv, boolean b) { 
		try {
			server=serv;
			name=n;
			sourceFile = bDir.getCanonicalFile(); 
			flag=true;
			keepAlife=b;
		} 
		catch (IOException ex) {
			System.out.println ("Error constructor ThreadServ: "+ ex); 
		}
		if (indexFich != null) 
			indexFileName= indexFich;
	}

	/**
	 * @funcion Se inicializa la variable root, que indicara la ruta del directorio del servidor. 
	 * 			Mientras el flag keepAlife sea true, se ejecutará el bucle donde, flag, que indica si la peticion
	 * 			es http get se gestiona la respuesta al cliente.
	 * 			Despues mientras la lista pool, este vacia, el hilo ejetucará un wait, y esperá a que el maestro reciba
	 * 			una peticion. En el momento que se coja una petición, se coge el socket de la lista, se conecta, y se gestiona
	 * 			la petición debidamente y se desconecta el socket.
	 */
	public void run(){
		try{
			root = sourceFile.getPath();

			while (keepAlife) {
				flag=true;
				while (server.pool.isEmpty()) {
					synchronized(server.pool){
						server.pool.wait();
					}
				}
				socket=server.pool.poll();
				conect();
				getClient();
				if(flag){
					sendClient();
				}
				disconnect();
			}	
		}
		catch(Exception e){
			System.err.println("Error en el run del hilo. "+e);
		}
	}

	/**
	 * @funcion Lee la petición HTTP recibida, muestra el nombre del esclavo que la atiende, seguida de la direccion
	 * 			del cliente que la envía y el contenido de la misma.
	 * 			Despues mientras la petición no sea null o favicon, se gestionará, de no ser así se sale del método
	 * 			y se desconectará, dando fin a la petición.
	 */
	public void getClient(){
		try{
			get=entrada.readLine();
			if(get==null){
				flag=false;
				System.out.println(name+"| "+"Recibido de "+socket.getInetAddress()+": "+ get);
				return;
			}
			if(get.contains("favicon")){
				flag=false;
				System.out.println(name+"| "+"Recibido de "+socket.getInetAddress()+": "+ get);
				return;
			}
			System.out.println(name+"| "+"Recibido de "+socket.getInetAddress()+": "+ get);
		}
		catch(Exception e){
			System.err.println("Error en getClient "+e);
		}
	}

	/**
	 * @funcion Se separa la petición, verificando de nuevo que la peticion es GET, si no, se notifica el error,
	 * 			En caso de que sea correcto lo anteior, se extrae el nombre del fichero de la petición, y se crea
	 *  		un objeto File con dicho nombre. Si el nombre del fichero es "dir", se muestra el contenido del servidor.
	 *  		Si el fichero existe, si esta en el servidor, se procede a enviar la cabecera MIME que confirma y que da
	 *  		comienzo a la transferencia del mismo. Si no se encuentra el fichero, se notifica el error.
	 */
	public void sendClient(){
		int tamano=0;
		String filename, contentType= "text/plain";

		try{
			if ((get.split(" "))[0].equals("GET")) {
				filename = (get.split(" "))[1];
				if (filename.endsWith("/"))
					filename+= indexFileName; 
				contentType = guessContentTypeFromName(filename);
				File theFile = new File(sourceFile,filename.substring(1,filename.length()));

				if(theFile.getName().equals("dir")){
					out.write("<HTML>\r\n");
					out.write("<HEAD><TITLE>Directorio</TITLE>\r\n");
					out.write("</HEAD>\r\n");
					out.write("<BODY>");
					out.write("<ul>");
					out.write("<p><strong><u>DIRECTORIO:</u></strong> "+root+"</p>\r\n");
					dir(root);
					out.write("</ul>");
					out.write("</BODY></HTML>\r\n ");
					out.flush();
				}
				else if (theFile.canRead() && theFile.getCanonicalPath().startsWith(root)) {
					DataInputStream fis = new DataInputStream(new BufferedInputStream(new FileInputStream(theFile)));
					byte[] theData = new byte[(int) theFile.length()]; 
					tamano= theData.length; 
					fis.readFully(theData);
					fis.close();
					cabeceraMIME (out, "200 OK", tamano, contentType);
					raw.write(theData); 
					raw.flush();
				} 
				else { 
					cabeceraMIME (out, "404 File Not Found", tamano, contentType); 
					tamano= 119;
					contentType= "text/html";
					cabeceraMIME (out, "200 OK", tamano, contentType);
					out.write("<HTML>\r\n");
					out.write("<HEAD><TITLE>File Not Found</TITLE>\r\n");
					out.write("</HEAD>\r\n");
					out.write("<BODY>");
					out.write("<H1>HTTP Error 404: File Not Found</H1>\r\n"); 
					out.write("</BODY></HTML>\r\n ");
					out.flush();
				}
			}
			else {
				cabeceraMIME (out, "501 Not Implemented", tamano, contentType); 
			}
		} 
		catch (SocketException s){}
		catch (IOException ex) {
			System.err.println("Error en el envío del fichero. "+ex);
		} 

	}

	public String guessContentTypeFromName(String name) {
		if (name.endsWith(".html") || name.endsWith(".htm")) {
			return "text/html";
		} else if (name.endsWith(".txt") || name.endsWith(".java")) {
			return "text/plain";
		} else if (name.endsWith(".gif")) {
			return "image/gif";
		} else if (name.endsWith(".jpg") || name.endsWith(".jpeg")) {
			return "image/jpeg";
		} else if (name.endsWith(".class")) {
			return "application/octet-stream"; } else return "text/plain";
	} 

	public void cabeceraMIME (PrintWriter wout, String resp, int tamano, String ctype){
		wout.println("HTTP/1.0 " + resp);
		Date now = new Date(tamano);
		wout.println("Date: " + now);
		wout.println ("Server: jhttp1/1.0"); 
		if (resp.indexOf("200 OK")>=0)
			wout.println("Content-length: "+ tamano);
		wout.println("Content-type: "+ ctype); 
		wout.println();
		wout.flush();
	}

	/**
	 * @funcion Se lista de forma recursiva y en HTTML el contenido del servidor, de tal manera que el directorio son pequeños
	 * 			titulos y los ficheros van listados.
	 * @param dir recibe el nombre del directorio que explorar
	 */
	public void dir(String dir){
		File directorio=new File (dir);
		File contenido[]= directorio.listFiles();
		for(int i=0;i<contenido.length;i++){
			if(contenido[i].isFile()){
				out.write("<ul><li>Fichero: " + contenido[i].getName()+ " || Peso: "+contenido[i].length()+"</li></ul>\r\n");
			}
		}
		for(int i=0;i<contenido.length;i++){
			if(contenido[i].isDirectory()){
				out.write("<p><strong><u>DIRECTORIO:</u></strong> " + contenido[i]+"</p>\r\n");
				dir(contenido[i].toString());
			}
		}
	}

	/**
	 * @funcion inicializa los flujos de lectura y escritura sobre el socket necesarios.
	 */
	public void conect(){
		try{ 
			entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			raw=new BufferedOutputStream(socket.getOutputStream());
			out=new PrintWriter(raw);
			in=new InputStreamReader(new BufferedInputStream(socket.getInputStream()),"ASCII"); 
		}
		catch(Exception e){
			System.err.println("Error conect. "+e);
		}
	}

	/**
	 * @funcion se cierran primero los flijos abiertos y posteriormente se cierra el socket de la petición.
	 */
	public void disconnect(){
		try{
			out.close();
			raw.close();
			in.close();
			socket.close();
		}
		catch(Exception e){
			System.err.println("Error disconnect: "+e);
		}
	}
}
